import aiosqlite
import os

DATABASE_NAME = f"{os.path.realpath(os.path.dirname(__file__))}/database/bot.db"

async def create_database():
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute('''CREATE TABLE IF NOT EXISTS users
                 (name text, 
                  nicknames text,
                  facts text,
                  discord_id integer PRIMARY KEY)''')
    await c.execute('''CREATE TABLE IF NOT EXISTS conversations
                 (id integer PRIMARY KEY AUTOINCREMENT,
                  user text,
                  message text,
                  async timestamp datetime DEFAULT CURRENT_TIMESTAMP)''')
    await conn.commit()
    await conn.close()

async def create_user(user):
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("INSERT INTO users (name, nicknames, facts, discord_id) VALUES (?, ?, ?, ?)",
        (user.name, ','.join(user.nicknames) if user.nicknames else '', ','.join(user.facts) if user.facts else '', user.discord_id))
    await conn.commit()
    await conn.close()

async def update_user_db(user):
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("UPDATE users SET name=?, nicknames=?, facts=? WHERE discord_id=?",
        (user.name, ','.join(user.nicknames) if user.nicknames else '', ','.join(user.facts) if user.facts else '', user.discord_id))
    await conn.commit()
    await conn.close()

async def update_user_facts(discord_id, facts):
    facts_string = ','.join(facts)
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("UPDATE users SET facts=? WHERE discord_id=?", (facts_string, discord_id))
    await conn.commit()
    await conn.close()

async def update_user_name(discord_id, name):
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("UPDATE users SET name=? WHERE discord_id=?", (name, discord_id))
    await conn.commit()
    await conn.close()

async def update_user_nicknames(discord_id, nicknames):
    nicknames_string = ','.join(nicknames)
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("UPDATE users SET nicknames=? WHERE discord_id=?", (nicknames_string, discord_id))
    await conn.commit()
    await conn.close()

async def get_all_users_db():
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("SELECT * FROM users")
    users_data = await c.fetchall()
    await conn.close()
    users = []
    for user_data in users_data:
        users.append({
            "name": user_data[0],
            "nicknames": user_data[1].split(','),
            "facts": user_data[2].split(','),
            "discord_id": user_data[3],
        })
    return users

async def get_users_by_name_db(name):
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("SELECT * FROM users WHERE LOWER(name)=LOWER(?)", (name.lower(),))
    user_data = await c.fetchall()
    await conn.close()
    users = []
    for data in user_data:
        user = {
            "name": data[0],
            "nicknames": data[1].split(','),
            "facts": data[2].split(','),
            "discord_id": data[3],
        }
        users.append(user)
    if users:
        return users
    else:
        return None

async def get_users_by_nickname_db(nickname):
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("SELECT * FROM users WHERE LOWER(nicknames) LIKE LOWER(?)", ('%'+nickname.lower()+'%',))
    user_data = await c.fetchone()
    await conn.close()
    if user_data is None:
        return None
    users = []
    for data in user_data:
        user = {
            "name": data[0],
            "nicknames": data[1].split(','),
            "facts": data[2].split(','),
            "discord_id": data[3]
        }
        users.append(user)
    if users:
        return users

async def get_user_by_discord_id_db(discord_id):
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("SELECT * FROM users WHERE discord_id=?", (discord_id,))
    user_data = await c.fetchone()
    await conn.close()
    if user_data:
        return {
            "name": user_data[0],
            "nicknames": user_data[1].split(','),
            "facts": user_data[2].split(','),
            "discord_id": user_data[3]
        }
    else:
        return None

async def add_conversation(user, message):
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("INSERT INTO conversations (user, message) VALUES (?, ?)", (user, message))
    await conn.commit()
    await conn.close()

async def delete_all_conversations():
    conn = await aiosqlite.connect(DATABASE_NAME)
    c = await conn.cursor()
    await c.execute("DELETE FROM conversations")
    conn.commit

async def delete_user(discord_id):
    async with aiosqlite.connect(DATABASE_NAME) as db:
        cursor = await db.cursor()
        await cursor.execute("DELETE FROM users WHERE discord_id=?", (discord_id,))
        await db.commit()