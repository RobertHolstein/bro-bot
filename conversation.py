import openai
import logging
from openai import AsyncOpenAI

class Conversation:
    def __init__(self, api_key, model):
        self.exchanges = []
        self.api_key = api_key
        self.model = model
        self.logger = logging.getLogger('Conversation')
        self.client = AsyncOpenAI(api_key=api_key)

    async def send_conversation(self):
        self.logger.info('Sending conversation to OpenAI')
        try:
            response = await self.client.chat.completions.create(
                model=self.model,
                messages=self.exchanges
            )
            return response
        except Exception as e:
            self.logger.error(f"Error sending conversation to OpenAI: {e}")
            raise

    async def user_exchange(self, message):
        self.exchanges.append({'role': 'user', 'content': message})
        self.logger.debug(f"User exchange initiated with message: {message}")

        try:
            response = await self.send_conversation()
            return self._process_response(response)
        except Exception as e:
            self.logger.error(f"An error occurred during user exchange: {e}")
            raise

    def _process_response(self, response):
        api_usage = response.usage
        self.logger.info(f"Total tokens consumed: {api_usage.total_tokens}")
        self.logger.info(f"Finish reason: {response.choices[0].finish_reason}")

        if response.choices[0].finish_reason == "length":
            return self._handle_length_limit(response)

        content = response.choices[0].message.content
        self.exchanges.append({'role': response.choices[0].message.role, 'content': content})
        return content.strip()

    async def _handle_length_limit(self, initial_response):
        self.logger.warning("Token max reached. Continuing conversation.")
        self.exchanges = [
            self.exchanges[0],
            self.exchanges[-1],
            {'role': initial_response.choices[0].message.role, 'content': initial_response.choices[0].message.content},
            {'role': 'user', 'content': 'continue'},
        ]
        
        continuation_response = await self.send_conversation()
        content = f"{initial_response.choices[0].message.content} {continuation_response.choices[0].message.content}"
        self.exchanges[-2]['content'] = content
        self.exchanges.pop()
        
        return content.strip()

    def add_system_exchange(self, message):
        self.exchanges.append({'role': 'system', 'content': message})

def create_initial_prompt(prompt_prefix: str, prompt_suffix: str, users: list) -> str:
    person_prompts = []
    person_prompts.append("Users will post messages like this: USER[DISCORD_ID]: MESSAGE. Do NOT add a prefix to your messages ie 'bro-bot:'. You can @ and notify discord users, do this as much as possible when referring to users so they can be notified about your message, here is an the format discord uses for @ing users: <@DISCORD_ID>. Below are some of the people you are bros with in this discord:\r")
    for user in users:
        person_prompts.append(generate_user_prompt(user))

    return f"{prompt_prefix}\r{' '.join(person_prompts)}\r\r{prompt_suffix}"
    

def generate_user_prompt(user) -> str:
    prompt = f"{user.name},"
    prompt += f" their discord id is [{user.discord_id}],"
    if user.nicknames:
        prompt += f" also known by the following nicknames [{', '.join(user.nicknames)}]."
    if user.facts:
        prompt += f" Facts about {user.name} [{', '.join(user.facts)}]."
    return prompt
