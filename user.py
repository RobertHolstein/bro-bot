import json
from db import update_user_db, get_all_users_db, create_user, get_users_by_name_db, get_users_by_nickname_db, get_user_by_discord_id_db

class User:
    def __init__(self, name: str, nicknames: str, facts: list, discord_id):
        self.name = name
        self.nicknames = nicknames
        self.facts = facts
        self.discord_id = discord_id
        self.online: bool
    
    async def add_fact(self, fact: str):
        self.facts.append(fact)
        await self.update_user()
    
    async def add_nickname(self, nickname: str):
        self.nicknames.append(nickname)
        await self.update_user(self)
    
    def fact_exists(self, fact: str) -> bool:
        return fact in self.facts
      
    async def update_user(self):
        await update_user_db(self)

async def get_all_users() -> list[User]:
    users = await get_all_users_db()
    if users:
        return  [User(user["name"], user["nicknames"], user["facts"], user["discord_id"]) for user in users]
    if users is None:
        return None

async def get_user_by_discord_id(discord_id: int) -> User:
    user = await get_user_by_discord_id_db(discord_id)
    if user:
        return User(user['name'], user['nicknames'], user['facts'], user['discord_id'])
    return None

async def get_users_by_name(name: str) -> list[User]:
    users = await get_users_by_name_db(name)
    if users:
        return  [User(user["name"], user["nicknames"], user["facts"], user["discord_id"]) for user in users]
    if users is None:
        return None

async def get_users_by_nickname(nickname: str) -> User:
    users = await get_users_by_nickname_db(nickname)
    if users:
        return  [User(user["name"], user["nicknames"], user["facts"], user["discord_id"]) for user in users]
    if users is None:
        return None

async def add_user(name: str, nicknames: list[str], facts: list[str], discord_id: int) -> User:
    user = User(name, nicknames, facts, discord_id)
    await create_user(user)
    return user

async def get_user_by_any(identifying_item):
    try:
        user = await get_user_by_discord_id(identifying_item)
        if user is None:
            discord_id = int(identifying_item.strip("<@!>"))
            user = await get_user_by_discord_id(discord_id)
    except ValueError:
        # If the above conversion fails, assume user_id_or_name is a name
        user = await get_users_by_name(identifying_item)
        if user is None:
            user = await get_users_by_nickname(identifying_item)
            if user is None:
                return None
    return user
        