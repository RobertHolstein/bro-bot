import os
# from dotenv import load_dotenv
import asyncio
import discord
from discord.ext import commands, tasks
import re
from db import *
from user import *
from conversation import *
import logging

logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

user_file = "users.json"

PROMPT_PREFIX = os.getenv("PROMPT_PREFIX")
PROMPT_SUFFIX = os.getenv("PROMPT_SUFFIX")

API_KEY = os.getenv("API_KEY")
MODEL = os.getenv("MODEL")
BOT_TOKEN = os.getenv("BOT_TOKEN")
CHANNEL_ID = int(os.getenv("CHANNEL_ID"))
VOICE_CHANNEL_ID = int(os.getenv("VOICE_CHANNEL_ID"))
FACT_VOTES_NEEDED = int(os.getenv("FACT_VOTES_NEEDED"))
BOT_NAMES = (os.getenv("BOT_NAMES")).split(",")

intents = discord.Intents.all()
bot = commands.Bot(command_prefix='%', intents=intents)

conversation = Conversation(API_KEY, MODEL)

@bot.command()
async def add_me(ctx, name: str, *nicknames: str):
    discord_name = ctx.author.name
    discord_id = ctx.author.id
    if nicknames:
        nicknames = nicknames.split(",")
        nicknames = [n.strip() for n in nicknames]
    else:
        nicknames = []
    user = add_user(name, [discord_name] + list(nicknames), [], discord_id)
    await ctx.send(f":heavy_plus_sign: {user.name} has been added to the user list. :heavy_plus_sign:")
    await notify_bot_new_user(user)
    logging.info(f"Adding user {name} with nicknames {nicknames}")

@bot.command()
async def add(ctx, discord_id: str, name: str, nicknames: str = [], facts: str = []):
    try:
        discord_id = int(discord_id)
    except ValueError:
        discord_id = int(discord_id.strip("<@!>"))

    user = await get_user_by_discord_id(discord_id)
    if user is not None:
        await ctx.send(f":octagonal_sign: User {user.name} [{discord_id}] already exists :octagonal_sign:")
        return
    guild = ctx.guild
    member = guild.get_member(discord_id)
    if member is None:
        await ctx.send(f":octagonal_sign: Could not find user with id [{discord_id}] in this server :octagonal_sign:")
        return
    if facts:
        facts = facts.split(",")
        facts = [f.strip() for f in facts]
    if nicknames:
        nicknames = nicknames.split(",")
        nicknames = [n.strip() for n in nicknames]
    new_user = await add_user(name, nicknames, facts, discord_id)
    await ctx.send(f":heavy_plus_sign: {new_user.name} has been added to the user list. :heavy_plus_sign:")
    await notify_bot_new_user(new_user)
    logging.info(f"Adding user {name} with ID {discord_id}, nicknames {nicknames}, facts {facts}")

@bot.command()
async def fact(ctx, discord_user_tag_or_name: str, fact: str):
    logging.info(f"Fact command invoked for user {discord_user_tag_or_name} with fact {fact}")
    
    user = await get_user_by_any(discord_user_tag_or_name)
    if user is None:
        await ctx.send(":octagonal_sign: User not found :octagonal_sign:")
        logging.warning(f"User {discord_user_tag_or_name} not found when trying to add fact")
        return
    user = await check_user_for_multi_and_same(user, ctx)

    try:
        if user.fact_exists(fact):
            await ctx.send(f":interrobang: This fact already exists for {user.name} [{user.id}] :interrobang:")
            logging.info(f"Fact already exists for user {user.name} with ID {user.id}")
            return
        await ctx.send(f"The following fact has been suggested for {user.name} [{user.discord_id}]\r\r{fact}\r\rVote with :white_check_mark: to add this fact or :x: to reject.")
        async for msg in ctx.channel.history(limit=1):
            message = msg
        await message.add_reaction("✅")
        await message.add_reaction("❌")
    except Exception as e:
        logging.error(f"An error occurred in the fact command: {e}")
        raise(e)


@bot.event
async def on_reaction_add(reaction, user):
    try:
        if user == bot.user:
            return
        message = reaction.message
        if 'The following fact has been suggested for' in message.content:
            user_id = int(re.search(r'\[(\d+)\]', message.content).group(1))
            user = await get_user_by_discord_id(user_id)
            if user is None:
                await message.channel.send("No user found with id [{user_id}]")
                return
            fact = message.content.split("\r\r")[1]
            if user.fact_exists(fact):
                await message.channel.send(f":interrobang: This fact already exists for {user.name} [{user.id}] :interrobang:")
                return
            if message.channel.id != CHANNEL_ID:
                return
            if (message.reactions[1].count - 1) == FACT_VOTES_NEEDED:
                await message.channel.send(":thumbsdown: Fact has been rejected :thumbsdown:")
                return
            if reaction.emoji == "✅":
                if (message.reactions[0].count - 1) == FACT_VOTES_NEEDED:
                    await user.add_fact(fact)
                    await message.channel.send(":thumbsup: Fact has been added :thumbsup:")

                    logging.info(f"Fact added for user: {user.name} [{user_id}]")
                    logging.debug(f"Added fact: {fact}")
                    
                    convo_message = f"\r\r{user.name}[{user.discord_id}] has had the following fact updated about them. Say something about this and tag them.\r\rFact: {fact}"
                    return_message = await conversation.user_exchange(convo_message)
                    await message.channel.send(return_message)
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e


@bot.event
async def on_message(message):
    if message.author == bot.user:
        return
    try:
        await bot.process_commands(message)
        if not message.content.startswith('%'):
            bot_mentioned = bot.user.mention in message.content
            if  bot_mentioned or any(keyword in message.content.lower() for keyword in BOT_NAMES):
                user = await get_user_by_discord_id(message.author.id)
                user_message = f"{user.name}[{user.discord_id}]: {message.content}"
                return_message = await conversation.user_exchange(user_message)
                channel = await bot.fetch_channel(message.channel.id)            
                limit = 2000
                for i in range(0, len(return_message), limit):
                    await channel.send(return_message[i:i + limit])

                logging.info("Received message")
                logging.debug(f"Message content: {message.content}")
                logging.debug(f"User: {user.name} [{user.discord_id}]")
                logging.debug(f"Returned message: {return_message}")
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e

@bot.event
async def on_voice_state_update(member, before, after):
    try:
        if before.channel != after.channel:
            if after.channel is not None:
                logging.info(f'{member.name} joined {after.channel.name}')
                if after.channel.id == VOICE_CHANNEL_ID:
                    user = await get_user_by_discord_id(member.id)
                    if user is None:
                        user = await create_user_from_discord_info(member)
                        await notify_bot_new_user(user)
                    else:
                        await send_user_greeting(user)
            elif before.channel is not None:
                logging.info(f'{member.name} left {before.channel.name}')
                if after.channel.id == VOICE_CHANNEL_ID:
                    user = await get_user_by_discord_id(member.id)

                    # Add logging statement
                    logging.debug(f'User: {user.name} [{user.discord_id}]')

    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e

@bot.listen()
async def on_ready():
    try:
        logging.info("Bot ready!")
        users = await get_all_users()
        prompt = create_initial_prompt(PROMPT_PREFIX, PROMPT_SUFFIX, users)
        conversation.add_system_exchange(prompt)

        voice_channel = await bot.fetch_channel(VOICE_CHANNEL_ID)
        channel = await bot.fetch_channel(CHANNEL_ID)
        members = voice_channel.members
        await update_online_members(members)
        await channel.send("bro-bot online")

    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e

async def send_user_greeting(user):
    try:
        message = f"{user.name}[{user.discord_id}] joined the voice channel. Greet them. You need to @ them."
        return_message = await conversation.user_exchange(message)
        channel = await bot.fetch_channel(CHANNEL_ID)
        await channel.send(return_message)
        
        # Add logging statement
        logging.info(f"Greeting sent to user: {user.name} [{user.discord_id}]")
    
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e

async def create_user_from_discord_info(member):
    try:
        user = await get_user_by_discord_id(member.id)
        if user is None:
            user = await add_user(member.name, [], [], member.id)
            await notify_bot_new_user(user)
        return user
    
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e

async def update_online_members(members):
    try:
        for member in members:
            await create_user_from_discord_info(member)
    
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e

async def notify_bot_new_user(user):
    try:
        message = f"The following user has been added to your knowledge base. Say something to them. You need to @ them."
        person_prompt = generate_user_prompt(user)
        message += person_prompt
        return_message = await conversation.user_exchange(message)
        channel = await bot.fetch_channel(CHANNEL_ID)
        await channel.send(return_message)
        
        # Add logging statement
        logging.info(f"Notified about new user: {user.name} [{user.discord_id}]")
    
    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e

async def check_user_for_multi_and_same(user, ctx):
    try:
        if isinstance(user, list):
            if len(user) > 1:
                await ctx.send(f":octagonal_sign: Multiple people with the name {user[0].name}. Rewrite your request with one of the following users' nicknames or IDs in the first field. :octagonal_sign:")
                for u in user:
                    await ctx.send(f":point_right:\tName: {u.name}\r\t\t  Nicknames: {', '.join(u.nicknames)}\r\t\t  ID: {u.discord_id}")
                return
            else:
                return user[0]
        return user

    except Exception as e:
        logging.error(f"An error occurred: {e}")
        raise e

if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(bot.start(BOT_TOKEN))
    loop.run_forever()